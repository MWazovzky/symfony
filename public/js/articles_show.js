$(document).ready(function () {
    $('.js-like-article').on('click', function (e) {
        e.preventDefault();
        let link = $(e.currentTarget);
        link.toggleClass('far').toggleClass('fas');

        let url = link.attr('href')
        $.ajax({
            method: 'POST',
            url,
        }).done(function (data) {
            $('.js-like-article-count').html(data.likes);
        });
    });
});
