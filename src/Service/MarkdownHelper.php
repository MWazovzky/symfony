<?php

namespace App\Service;

use Knp\Bundle\MarkdownBundle\MarkdownParserInterface;
use Symfony\Component\Cache\Adapter\AdapterInterface;

class MarkdownHelper
{
    protected $parser;

    protected $cache;

    public function __construct(MarkdownParserInterface $parser,  AdapterInterface $cache)
    {
        $this->parser = $parser;
        $this->cache = $cache;
    }

    public function process(string $content): string
    {
        $item = $this->cache->getItem('markdown_'.md5($content));

        if (!$item->isHit()) {
            $item->set($this->parser->transform($content));
            $this->cache->save($item);
        }

        return $item->get();
    }
}
