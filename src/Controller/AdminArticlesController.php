<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Article;
use Doctrine\ORM\EntityManagerInterface;

class AdminArticlesController extends AbstractController
{
    /**
     * @Route("/admin/articles/store", name="articles_admin")
     */
    public function store(EntityManagerInterface $em)
    {
        $content = <<<EOF
Lorem ipsum dolor,  sit amet **consectetur adipisicing** elit. Deleniti voluptas suscipit 
beatae quis quibusdam debitis ipsa illum, aliquam ullam totam commodi fugit explicabo 
modi voluptatum unde [google](https://google.com) laudantium itaque repellendus!

Blanditiis quaerat sint maiores fuga sapiente impedit similique harum facilis voluptates
est, tenetur, accusamus sunt voluptatem voluptatum nihil repellat dolores delectus 
placeat quos sed! Architecto voluptatum iure recusandae id enim fuga dicta sunt 
aspernatur! 

Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorem hic, aut odit quas 
numquam eos ab! Atque est debitis voluptatum aut commodi assumenda, eaque modi ab 
earum excepturi doloribus in. Perferendis, natus amet? 
EOF;

        $article = new Article;
        $article->setTitle('Dummy article')
                ->setSlug('dummy-article-' . rand(100, 999))
                ->setContent($content)
                ->setAuthor('Rendall Bogg')
                ->setLikesCount(rand(5, 100))
                ->setImageFile('boats.jpg');

        if (rand(1 ,10) > 3) {
            $article->setPublishedAt(new \DateTime(sprintf('-%d days', rand(1, 100))));
        }

        $em->persist($article);
        $em->flush();

        return new Response(sprintf(
            'New article has been published. id: %d, slug: \'%s\'',
            $article->getId(),
            $article->getSlug()
        ));
    }
}
