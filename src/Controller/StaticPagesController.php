<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class StaticPagesController
{
    /**
     * @Route("/")
     */
    public function index()
    {
        return new Response('Home Page');
    }

    /**
     * @Route("/about")
     */
    public function about()
    {
        return new Response('About Page');
    }

    /**
     * @Route("/contacts")
     */
    public function contacts()
    {
        return new Response('Contacts Page');
    }
}
