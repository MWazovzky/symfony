<?php

namespace App\Controller;

use App\Repository\CommentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Knp\Component\Pager\PaginatorInterface;

class AdminCommentsController extends AbstractController
{
    /**
     * @Route("/admin/comments", name="admin_comments")
     */
    public function index(CommentRepository $repository, PaginatorInterface $paginator, Request $request)
    {
        $q = $request->query->get('q');

        $query = $repository->getSearchQuery($q);

        $pagination = $paginator->paginate(
            $query, /* query builder object */
            $request->query->getInt('page', 1), /* page number */
            10 /* limit per page */
        );

        return $this->render('admin_comments/index.html.twig', [
            'pagination' => $pagination,
            'q' => $q,
        ]);
    }
}
