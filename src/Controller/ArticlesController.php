<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Psr\Log\LoggerInterface;
use App\Service\SlackClient;
use App\Repository\ArticleRepository;
use App\Entity\Article;
use Doctrine\ORM\EntityManagerInterface;

class ArticlesController extends AbstractController
{
    /**
     * @Route("/articles", name = "articles_index")
     */
    public function index(ArticleRepository $repository)
    {
        $articles = $repository->findPublishedArticlesOrderedByPublishedAt();

        return $this->render('articles/index.html.twig', [
            'articles' => $articles,
        ]);
    }

    /**
     * @Route("/articles/{slug}", name = "articles_show")
     */
    public function show(Article $article)
    {
        return $this->render('articles/show.html.twig', [
            'article' => $article,
        ]);
    }

    /**
     * @Route("/articles/{slug}/likes", name = "articles_toggle_likes", methods={"POST"})
     */
    public function toggleLikes(Article $article, LoggerInterface $logger, SlackClient $slack, EntityManagerInterface $em)
    {
        $article->incrementLikesCount();
        $em->flush();

        $slack->sendMessage('Mike Wazovzky', 'Your article has been liked.');

        $logger->info('Article has been liked.');

        return $this->json(['likes' => $article->getLikesCount()]);
    }
}
