<?php

namespace App\DataFixtures;

use App\DataFixtures\BaseFixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Comment;
use App\Entity\Article;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class CommentFixture extends BaseFixture implements DependentFixtureInterface
{
    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(Comment::class, 100, function (Comment $comment, $count) use ($manager) {
            $comment->setContent($this->faker->boolean ? $this->faker->paragraph : $this->faker->sentence(2, true));
            $comment->setAuthorName($this->faker->name);
            $comment->setCreatedAt($this->faker->dateTimeBetween('-1 months', '-1 seconds'));
            $comment->setIsDeleted($this->faker->boolean(20));
            $comment->setArticle($this->getRandomReference(Article::class));
        });
    }

    /**
     * Specify fixtures that should be loaded before this one.
     */
    public function getDependencies()
    {
        return [ArticleFixture::class];
    }
}
