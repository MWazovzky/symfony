<?php

namespace App\DataFixtures;

use App\DataFixtures\BaseFixture;
use App\DataFixtures\TagFixture;
use App\Entity\Article;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use App\Entity\Tag;

class ArticleFixture extends BaseFixture implements DependentFixtureInterface
{
    protected static $articleTitles = [
        'Vue testing with Jest',
        'Functional Components in Vue.js',
        'Deploy Laravel project to shared hosting',
        'Symfony 4: Best Practices',
    ];

    protected static $articleAuthors = [
        'Mike Wazovzky',
        'James P. Sullivan',
        'Celia Mae',
        'Randell Boggs',
        'Boo',
    ];

    protected static $articleImages = [
        'boats.jpg',
        'venice.jpg',
        'formenterra.jpg',
        'skyisland.jpg',
    ];

    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(Article::class, 10, function (Article $article, $count) use ($manager) {
            $content = <<<EOF
Lorem ipsum dolor,  sit amet **consectetur adipisicing** elit. Deleniti voluptas suscipit 
beatae quis quibusdam debitis ipsa illum, aliquam ullam totam commodi fugit explicabo 
modi voluptatum unde [google](https://google.com) laudantium itaque repellendus!

Blanditiis quaerat sint maiores fuga sapiente impedit similique harum facilis voluptates
est, tenetur, accusamus sunt voluptatem voluptatum nihil repellat dolores delectus 
placeat quos sed! Architecto voluptatum iure recusandae id enim fuga dicta sunt 
aspernatur! 

Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorem hic, aut odit quas 
numquam eos ab! Atque est debitis voluptatum aut commodi assumenda, eaque modi ab 
earum excepturi doloribus in. Perferendis, natus amet? 
EOF;
            $article->setTitle($this->faker->randomElement(self::$articleTitles))
                ->setContent($content)
                ->setAuthor($this->faker->randomElement(self::$articleAuthors))
                ->setLikesCount($this->faker->numberBetween(5, 100))
                ->setImageFile($this->faker->randomElement(self::$articleImages));

            if ($this->faker->boolean(80)) {
                $article->setPublishedAt($this->faker->dateTimeBetween('-100 days', '-1 days'));
            }

            $tags = $this->getRandomReferences(Tag::class, $this->faker->numberBetween(0, 5));
            foreach ($tags as $tag) {
                $article->addTag($tag);
            }
        });
    }

    /**
     * Specify fixtures that should be loaded before this one.
     */
    public function getDependencies()
    {
        return [TagFixture::class];
    }
}
