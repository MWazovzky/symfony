<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;
use App\Service\MarkdownHelper;
use Symfony\Contracts\Service\ServiceSubscriberInterface;
use Psr\Container\ContainerInterface;

class AppExtension extends AbstractExtension implements ServiceSubscriberInterface
{
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
            new TwigFilter('cached_markdown', [$this, 'processMarkdown'],  ['is_safe' => ['html']]),
        ];
    }

    public function processMarkdown($value)
    {
        // $markdown = $this->container->get(MarkdownHelper::class);
        $markdown = $this->container->get('markdown_helper');
        return $markdown->process($value);
    }

    public static function getSubscribedServices()
    {
        return [
            // MarkdownHelper::class,
            'markdown_helper' => MarkdownHelper::class,
        ];
    }
}
