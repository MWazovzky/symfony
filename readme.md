## Doctrine commands
```
bin/console make:entity
bin/console make:migration
bin/console doctrine:migrations:migrate
bin/console doctrine:migrations:status
bin/console doctrine:database:drop --force
bin/console doctrine:database:create
```

## Use dependency injection via service subscription 
Avoid object instanciation if they are not required (lazy)
Required only in
- Twig Extentions,
- Event Subscribers,
- Security Voters 

## Doctrine Fixtures
```
bin/console make:fixtures
bin/console doctrine:fixtures:load
```

## Twig config && tools
[Twig Extentions](https://twig-extensions.readthedocs.io/en/latest/)
```
bin/console debug:twig
```
